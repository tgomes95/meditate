#!/usr/bin/env python

import os
import sys

from pdfrw import PdfReader, PdfWriter

def medidate(inpfn, outfn):
    trailer = PdfReader(inpfn)

    for k,v in trailer.Info.items():
        print(k, ':', v)

    print()

    option = None

    while option not in (6, 7):
        menu = (
            '1. Erase',
            '2. Edit Author',
            '3. Edit Title',
            '4. Print Metadata',
            '5. Save',
            '6. Save and Quit',
            '7. Quit',
        )

        print('\n'.join(menu))

        option = int(input('Option: '))

        print()

        if option == 1:
            trailer.Info = {}
            PdfWriter(outfn, trailer=trailer).write()
            trailer = PdfReader(outfn)

        elif option == 2:
            trailer.Info.Author = input('Author: ')
            print()

        elif option == 3:
            trailer.Info.Title = input('Title: ')
            print()

        elif option == 4:
            for k,v in trailer.Info.items():
                print(k, ':', v)
            print()

        elif option == 5:
            PdfWriter(outfn, trailer=trailer).write()

        elif option == 6:
            PdfWriter(outfn, trailer=trailer).write()

def main():
    if len(sys.argv) > 1:
        inpfn = sys.argv[1]

        if len(sys.argv) > 2:
            outfn = sys.argv[2].split('.')[0] + '.pdf'

        else:
            outfn = 'altered.pdf'

        medidate(inpfn, outfn)

    else:
        print('Missing arguments!')

if __name__ == '__main__':
    main()
